import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './views/index/index.component';
import { ResultsComponent } from './views/results/results.component';
import { PageNotFoundComponent } from './views/page-not-found/page-not-found.component';

const routes: Routes = [
    { path: '', component: IndexComponent, pathMatch: 'full'},
    { path: 'resultats', component: ResultsComponent},
	{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})

export class AppRoutingModule { }

export const RoutingComponents = [
    IndexComponent,
    ResultsComponent,
    PageNotFoundComponent
];
