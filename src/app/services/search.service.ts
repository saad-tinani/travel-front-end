import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
	providedIn: 'root'
})
export class SearchService {

	//private _url: string = "http://localhost:62360";
	
	private results: any[] = [];

	constructor(private _httpService: HttpClient) { }

	getOffersFromServer(searchData: any): Observable<any[]> {
		return this._httpService.post<any[]>("/api/search/getOffers", searchData)
			.pipe(
				catchError(
					// Catch the server side error
					this.handleError('Search Service - getOffers() method', [])
				)
			);
	}

	/*getCTMOffers(searchData: any): Observable<any[]> {
		let myHeaders = new HttpHeaders();
		myHeaders.set("Host", "billetterie.ctm.ma");
		myHeaders.set("Accept", "application/json");
		myHeaders.set("Access-Control-Allow-Origin", "*");
		myHeaders.set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

		return this._httpService.get<any[]>("http://billetterie.ctm.ma/site/select_voyages_aller?datev=" + searchData.DepartureDate + "&agen_dep=" + searchData.DepartureLocationCode + "&agen_dest=" + searchData.ArrivalLocationCode + "&nbp=1&aller=AS&tpay=MA&q=1&_search=false&nd=1513643188899&rows=30&page=1&sidx=&sord=", {headers: myHeaders})
			.pipe(
				catchError(
					// Catch the server side error
					this.handleError('Search Service - getOffers() method', [])
				)
			);
	}*/

	getResults(): any {
		return this.results;
	}

	setResults(results: any[]): void{
		this.results = results;
	}

	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			// this._router.navigate(["/administration/404"]);
			// (Historique) Display the error (Service - method)
			console.log(`${operation} failed: ${error.message}`);
			// Let the app keep running by returning an empty result.
			return of(result as T);

		};
	}

}
