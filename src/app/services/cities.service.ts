import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { City } from '../models/city.model';

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
	providedIn: 'root'
})
export class CitiesService {

    private _url: string = "http://localhost:62360";
    // private _url: string = "assets/data/cities.json";

	constructor(private _httpService: HttpClient) { }

	getCities(): Observable<City[]> {
		return this._httpService.get<City[]>("/api/city/get")
			.pipe(
				catchError(
					// Catch the server side error
					this.handleError('Cities Service - getCities() method', [])
				)
			);
	}

	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			// this._router.navigate(["/administration/404"]);
			// (Historique) Display the error (Service - method)
			console.log(`${operation} failed: ${error.message}`);
			// Let the app keep running by returning an empty result.
			return of(result as T);

		};
	}

}
