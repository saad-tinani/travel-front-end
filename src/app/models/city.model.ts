import { Deserializable } from "./deserializable.model";

export class City implements Deserializable {

    public Id: number;
    public Code: number;
    public Name: string;
    public Lat: number;
    public Lng: number;

    constructor(
        id: number = 0,
        code: number = 0,
        name: string = "",
        lat: number = 0,
        lng: number = 0
    ) {
        this.Id = id;
        this.Code = code,
        this.Name = name;
        this.Lat = lat;
        this.Lng = lng;
    }

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }

}