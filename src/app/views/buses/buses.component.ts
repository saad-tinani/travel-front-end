import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';

@Component({
	selector: 'buses',
	templateUrl: './buses.component.html',
	styleUrls: ['./buses.component.css']
})
export class BusesComponent implements OnInit {

	@Input('parentData') public busOffers;

	constructor() { }

	ngOnInit() {
	}

	getStopsNumber(busOffer: any): number {
		return busOffer.TripDetails.length - 1;
	}

	getTotalPrice(busOffer: any): number {

		var totalPrice: number = 0;

		busOffer.TripDetails.forEach(tripDetail => {
			totalPrice += tripDetail.Price;
		});

		return totalPrice;

	}

	getDepartureName(busOffer: any): string {
		return busOffer.TripDetails[0].DepartureLocation.Name;
	}

	getArrivalName(busOffer: any): string {
		return busOffer.TripDetails[busOffer.TripDetails.length-1].ArrivalLocation.Name;
	}

	getDepartureTime(busOffer: any): string {
		return busOffer.TripDetails[0].DepartureTime;
	}

	getArrivalTime(busOffer: any): string {
		return busOffer.TripDetails[busOffer.TripDetails.length-1].ArrivalTime;
	}

	getTotalDuration(departure_time: string, arrival_time: string): string {
		var duration = moment.duration(moment(arrival_time, "HH:mm").diff(moment(departure_time, "HH:mm")));
		console.log(duration.hours());
		return Math.sqrt(Math.pow(duration.hours(), 2)) + " Heures, " + Math.sqrt(Math.pow(duration.minutes(), 2)) + " Minutes";
	}

}
