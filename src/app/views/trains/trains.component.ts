import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';

@Component({
	selector: 'trains',
	templateUrl: './trains.component.html',
	styleUrls: ['./trains.component.css']
})
export class TrainsComponent implements OnInit {

	@Input('parentData') public trainOffers;

	constructor() { }

	ngOnInit() {
	}

	getStopsNumber(trainOffer: any): number {
		return trainOffer.TripDetails.length - 1;
	}

	getTotalPrice(trainOffer: any): number {

		var totalPrice: number = 0;

		trainOffer.TripDetails.forEach(tripDetail => {
			totalPrice += tripDetail.Price;
		});

		return totalPrice;

	}

	getDepartureName(trainOffer: any): string {
		return trainOffer.TripDetails[0].DepartureLocation.Name;
	}

	getArrivalName(trainOffer: any): string {
		return trainOffer.TripDetails[trainOffer.TripDetails.length-1].ArrivalLocation.Name;
	}

	getDepartureTime(trainOffer: any): string {
		return trainOffer.TripDetails[0].DepartureTime;
	}

	getArrivalTime(trainOffer: any): string {
		return trainOffer.TripDetails[trainOffer.TripDetails.length-1].ArrivalTime;
	}

	getTotalDuration(departure_time: string, arrival_time: string): string {
		var duration = moment.duration(moment(arrival_time, "HH:mm").diff(moment(departure_time, "HH:mm")));
		console.log(duration.hours());
		return Math.sqrt(Math.pow(duration.hours(), 2)) + " Heures, " + Math.sqrt(Math.pow(duration.minutes(), 2)) + " Minutes";
	}

}
