import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';

@Component({
	selector: 'planes',
	templateUrl: './planes.component.html',
	styleUrls: ['./planes.component.css']
})
export class PlanesComponent implements OnInit {

	@Input('parentData') public planeOffers;

	constructor() { }

	ngOnInit() {
	}

	getStopsNumber(planeOffer: any): number {
		return planeOffer.TripDetails.length - 1;
	}

	getTotalPrice(planeOffer: any): number {

		var totalPrice: number = 0;

		planeOffer.TripDetails.forEach(tripDetail => {
			totalPrice += tripDetail.Price;
		});

		return totalPrice;

	}

	getDepartureName(planeOffer: any): string {
		return planeOffer.TripDetails[0].DepartureLocation.Name;
	}

	getArrivalName(planeOffer: any): string {
		return planeOffer.TripDetails[planeOffer.TripDetails.length-1].ArrivalLocation.Name;
	}

	getDepartureTime(planeOffer: any): string {
		return planeOffer.TripDetails[0].DepartureTime;
	}

	getArrivalTime(planeOffer: any): string {
		return planeOffer.TripDetails[planeOffer.TripDetails.length-1].ArrivalTime;
	}

	getTotalDuration(departure_time: string, arrival_time: string): string {
		var duration = moment.duration(moment(arrival_time, "HH:mm").diff(moment(departure_time, "HH:mm")));
		console.log(duration.hours());
		return Math.sqrt(Math.pow(duration.hours(), 2)) + " Heures, " + Math.sqrt(Math.pow(duration.minutes(), 2)) + " Minutes";
	}

}
