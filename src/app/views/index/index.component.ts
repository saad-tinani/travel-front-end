import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CitiesService } from 'src/app/services/cities.service';
import { City } from 'src/app/models/city.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { SearchService } from 'src/app/services/search.service';
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
	selector: 'app-index',
	templateUrl: './index.component.html',
	styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

	cities: City[] = [];

	departure_location_id: number = null;
	arrival_location_id: number = null;

	departure_location_code: number = null;
	arrival_location_code: number = null;

	defaultTimeValue = new Date(0, 0, 0, 0, 0, 0);

	departure_date: Date | null = null;
	departure_time: Date | null = null;
	
	constructor(private _spinner: NgxSpinnerService,
				private _router: Router,
				private _citiesService: CitiesService,
				private _searchService: SearchService) { }

	ngOnInit() {

		// Get cities from the server
		this.getCitiesFromServer();

	}

	onChange(result: Date): void {
		console.log('onChange: ', moment(this.departure_date).format("DD-MM-YYYY").toString());
	}

	getCities(): City[] {
		return this.cities;
	}

	getCityById(city_id: Number): City {
		for (let i = 0; i < this.cities.length; i++) {
			if(this.cities[i].Id == city_id)
				return this.cities[i];
		}
		return null;
	}

	getCitiesFromServer(): void {

		this._spinner.show();

		this._citiesService.getCities().subscribe(
			(response) => {
				console.log(response);
				this.cities = response.map(city => {
					return new City(
						city.Id,
						city.Code,
						city.Name,
						city.Lat,
						city.Lng
					);
				});

				this._spinner.hide();
				
			},(error) => {
				this._spinner.hide();
				console.log("getCitiesFromServer() failed: ", error);
			}
		);

	}

	submit(): void {

		var departure_city: City;
		var arrival_city: City;
		var departure_date: string;

		this._spinner.show();

		departure_city = this.getCityById(this.departure_location_id);
		arrival_city = this.getCityById(this.arrival_location_id);
		departure_date = moment(this.departure_date).format("dd-MM-yyyy").toString();

		console.log("DepartureLocationId: ", this.departure_location_id)
		console.log("ArrivalLocationId: ", this.arrival_location_id)

		console.log("DepartureLocation: ", departure_city)
		console.log("ArrivalLocation: ", arrival_city)

		var searchData: any = {
			"DepartureLocationId": departure_city.Id,
			"ArrivalLocationId": arrival_city.Id,
			"DepartureLocationCode": departure_city.Code.toString(),
			"ArrivalLocationCode": arrival_city.Code.toString(),
			"DepartureDate": departure_date,
			"DepartureTime": this.departure_time.getHours().toString() + ":" + this.departure_time.getMinutes().toString()
		}	

		this._searchService.getOffersFromServer(searchData).subscribe(
			(response) => {

				console.log("CTMMMM: ", response);
				this._searchService.setResults(response);
				this._router.navigate(["/resultats"]);
				// this._spinner.hide();
				
			},(error) => {
				this._spinner.hide();
				console.log("getOffersFromServer() failed: ", error);
			}
		);

		/*this._searchService.getCTMOffers(searchData).subscribe((response) => {
			console.log("karim", response);
		});*/

	}

}
