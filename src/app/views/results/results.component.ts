import { Component, OnInit, ViewChild } from '@angular/core';
import { SearchService } from 'src/app/services/search.service';
import { NgxSpinnerService } from 'ngx-spinner';

// import { } from '@types/googlemaps';

@Component({
	selector: 'app-results',
	templateUrl: './results.component.html',
	styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {
	
	//__map div
	// @ViewChild('gmap') gmapElement: any

	//__map object
	// map: google.maps.Map;

	public tripType: number = 2;

	public results: any[] = [];

	public busOffers: any[] = [];
	public trainOffers: any[] = [];
	public planeOffers: any[] = [];

	public busOffersDetails: any[] = [];
	public trainOffersDetails: any[] = [];
	public planeOffersDetails: any[] = [];

	constructor(private _spinner: NgxSpinnerService,
		private _searchService: SearchService) { }

	ngOnInit() {

		/*this.map = new google.maps.Map(this.gmapElement.nativeElement, {
			center: new google.maps.LatLng(18.5793, 73.8143),
			zoom: 13,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});*/

		this.results = this._searchService.getResults();

		console.log('Results: ', this.results)

		this.busOffers = this._searchService.getResults().Voyages;
		console.log("CTM: ", this.busOffersDetails)

		this.trainOffersDetails = this._searchService.getResults().TripDetails.filter(offer => offer.Trip.Type == 2);
		this.trainsOffers();

		this.planeOffersDetails = this._searchService.getResults().TripDetails.filter(offer => offer.Trip.Type == 3);
		this.planesOffers();

		this._spinner.hide();

		

	}

	setTripType(tripType: number) {
		this.tripType = tripType;
	}

	getResultsNumber(): number {
		return this.busOffers.length + this.trainOffers.length + this.planeOffers.length;
	}

	getResults(): any[] {
		return this.results;
	}

	getBusOffers(): any[] {
		return this.busOffers;
	}

	getTrainOffers(): any[] {
		return this.trainOffers;
	}

	getPlaneOffers(): any[] {
		return this.planeOffers;
	}

	getBusOffersDetails(): any[] {
		return this.busOffersDetails;
	}

	getTrainOffersDetails(): any[] {
		return this.trainOffersDetails;
	}

	getPlaneOffersDetails(): any[] {
		return this.planeOffersDetails;
	}

	trainsOffers(): void {

		var tripsIds: number[] = [];
		
		this.trainOffersDetails.forEach(trainOfferDetail => {
			if(tripsIds.indexOf(trainOfferDetail.TripId) == -1) {

				var tripDetails: any[] = [];
				var trainTrip: any = null;

				tripsIds.push(trainOfferDetail.TripId);

				tripDetails = this.trainOffersDetails.filter(trainTripDetail => {
					return (trainTripDetail.TripId == trainOfferDetail.TripId)
				});

				trainTrip = {
					"Trip": trainOfferDetail.Trip,
					"TripDetails": tripDetails
				}

				console.log("trainTrip: ", trainTrip)

				this.trainOffers.push(trainTrip);

			}
		});

	}

	planesOffers(): void {

		var planesIds: number[] = [];
		
		this.planeOffersDetails.forEach(planeOfferDetail => {
			if(planesIds.indexOf(planeOfferDetail.TripId) == -1) {

				var tripDetails: any[] = [];
				var planeTrip: any = null;

				planesIds.push(planeOfferDetail.TripId);

				tripDetails = this.planeOffersDetails.filter(planeTripDetail => {
					return (planeTripDetail.TripId == planeOfferDetail.TripId)
				});

				planeTrip = {
					"Trip": planeOfferDetail.Trip,
					"TripDetails": tripDetails
				}

				this.planeOffers.push(planeTrip);

			}
		});

	}

}
