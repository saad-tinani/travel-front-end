import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { TrainsComponent } from './views/trains/trains.component';
import { PlanesComponent } from './views/planes/planes.component';
import { BusesComponent } from './views/buses/buses.component';
import { FormsModule } from '@angular/forms';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { NgxSpinnerModule } from 'ngx-spinner';

/** config angular i18n **/
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { RoutingComponents, AppRoutingModule } from './app-routing.module';
registerLocaleData(en);

@NgModule({
	declarations: [
		RoutingComponents,
		AppComponent,
		HeaderComponent,
		FooterComponent,
		TrainsComponent,
		PlanesComponent,
		BusesComponent
	],
	imports: [
		AppRoutingModule,
		BrowserModule,
		FormsModule,
		HttpClientModule,
		/** import ng-zorro-antd root module，you should import NgZorroAntdModule instead in sub module **/
		NgZorroAntdModule,
		NgxSpinnerModule,
		BrowserAnimationsModule
	],
	providers: [{ provide: NZ_I18N, useValue: en_US }],
	bootstrap: [AppComponent]
})
export class AppModule { }
